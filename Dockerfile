FROM alpine:latest
WORKDIR /documentation/
RUN mkdir -p /documentation/build

RUN apk add --no-cache python3 make git
RUN apk add py3-pip
RUN pip3 install git+https://github.com/sphinx-doc/sphinx && pip3 install sphinx-autobuild && pip3 install furo && pip3 install sphinx-copybutton

CMD sphinx-autobuild -b html --host 0.0.0.0 --port 80 /documentation /documentation/build