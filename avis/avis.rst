Avisierung
==========

Postgres-DB
-----------

Erstellung Datenmodell
^^^^^^^^^^^^^^^^^^^^^^
Zur Erstellung des Datenmodells soll das Skript aus folgendem Pfad verwendet werden:

.. code-block::

   %NIS_PRODUKTE%\nis_components\modules\nis_gss_tools\resources\base\db_scripts\stavi\stavi\create_stavi_schema.sql


Daten erfassen
^^^^^^^^^^^^^^

**Status**
Mit dem folgenden SQL-Skript können die Standarddaten (Status) importiert werden:

.. code-block::

   %NIS_PRODUKTE%\nis_components\modules\nis_gss_tools\resources\base\db_scripts\stavi\stavi\fill_stavi_schema.sql


**Text-Block**
Mit dem folgenden SQL-Befehl können benutzerdefinierte Text-Blöcke definiert werden:

.. code-block:: sql

   INSERT INTO stavi.textblock (title, text_de, text_fr)
   VALUES ('Normale Abstellung / Détachement normale', 'Es findet eine geplante Stromabstellung statt.', 'Il y a une coupure de courant prévue.');


.. warning:: Hinweis

   Dies ist ein Hinweis!

**Auftragsleiter**

Mit dem folgenden SQL-Befehl können benutzerdefinierte Auftragsleiter definiert werden:

.. code-block:: sql

   INSERT INTO stavi.projectleaders (name)
   VALUES ('Name');

Um einen Auftragsleiter als inaktiv zu setzen kann folgender Befehl verwendet werden:

.. code-block:: sql

   UPDATE stavi.projectleaders SET is_active=false WHERE name='Name'

**Gebiet**

Mit dem folgenden SQL-Befehl können benutzerdefinierte Gebiete definiert werden:

.. code-block:: sql

   INSERT INTO stavi.regions (name)
   VALUES ('Region');

Um ein Gebiet als inaktiv zu setzen, kann folgender Befehl verwendet werden:

.. code-block:: sql

   UPDATE stavi.regions SET is_active=false WHERE name='Region'

**Typ**

Mit dem folgenden SQL-Befehl können benutzerdefinierte Typendefiniert werden. Standardmässig sind bereits "Stromabschaltung" und "Baustelleninformation" vorhanden.

.. code-block:: sql

   INSERT INTO stavi.type(name)
   VALUES ('Stromabschaltung2');

Benutzer und Recht erstellen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Mit dem folgenden SQL-Skript wird das benötigte Recht erstellt:

.. code-block:: sql

   insert into nx.permission (id, permission_name, user_group, settings, required_module)
   values(14, 'has_avi', null, E'ui_prefs.nx.avi=true\npermissions.deny-avi=', 'AVI');

Prüfen ob in der PG-Datenbank der View nx.user_details_view vorhanden ist. Wenn nicht mit nachfolgendem SQL-Befehl erzeugen.

.. code-block:: sh

   --create nx.user_details_view for displayname
   CREATE VIEW nx.user_details_view
   AS
   SELECT u.id,
   u.principalid,
   coalesce(trim(coalesce(u.firstname, null) || coalesce(' ' || u.lastname, null)), u.principalid) displayname
   FROM nx.user_details u;

**gss-role-nisNx.properties**

Das permission setting mit dem Controller-Namen muss in *gss-role-nisNx.properties* wie folgt eingefügt werden:

.. code-block:: java

    published_settings.permissions.deny-avi=staviProject/.*

**Benutzerverwaltung**

Danach kann über die Benutzerverwaltung das "AVI"-Recht vergeben werden.

.. image:: avis_benutzerverwaltung.png

**grails_project.groovy**

Um das AVI-Module beim Kunden zu aktivieren, muss in der grails_project.groovy folgende Zeile eingetragen werden:

.. code-block:: java
    :linenos:

    nis.modules.AVI = true

Konfiguration Vektor-Layer und SMW-Thema *Stavi*
------------------------------------------------

Folgendes muss in der Grails Config (*grails_project.groovy*) für die Anzeige der Messstellen in der Karte konfiguriert werden:

.. code-block:: java
    :linenos:

    // Mapping auf die entsprechenden Kundenfelder der Messstellen in Smallworld.
    // Die Values sollen entsprechend angepasst werden, die Keys bleiben wie sie sind.
    nis.stavi.meterpoint_attribute_mapping = [
	    "nis_number": "nis_number",
	    "plz": "kf_ao_plz",
	    "community": "kf_ao_community",
	    "street": "kf_ao_street",
	    "street_addition"    : "kf_ao_street_addition", // (nur EKZ)
	    "house_number": "kf_ao_house_number",
	    "house_number_addition": "kf_ao_house_number_addition", // (nur EKZ)
	    "number_of_customers": "number_of_customer", // (nur EKZ)
	    "ao_id": "kf_ao_id",
	    "ao_art": "kf_ao_art" // (nur EKZ)
    ]

    // Maximaler Massstab in dem die Messstellen noch auf der Karte angezeigt werden
    nis.stavi.max_scale = 1000

Neue Services müssen in der *nis_gss_basic_config.xml* unter "nis_resource_configuration_{kundenname}\nis_gss_basic_application\resources\base\data" konfiguriert werden:

.. code-block:: xml

    <service name="feature_map" provider="map_service_provider.feature_map" />
    <service name="nis_feature_map" provider="nis_gss_stavi_map_service_provider.nis_feature_map" />
    <service name="get_stavi_objects_details" provider="nis_gss_stavi_object_details_service_provider.get_stavi_object_details" />

Neuen Service in den *gss-native_vs.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" eintragen:

.. code-block:: java

    gss.service.nis_map=ejb/NisMapLocal,nisFeatureMap
    gss.service.stavi = ejb/StaviObjectDetailLocal

Resource Handler für Symbole in den *gss-core.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" eintragen:

.. code-block::

    # Resource type "symbol" (Temporary Resource)
    gss.resourcehandler.symbol.rootpath=D:/WebDevPlatform/resource/symbol
    gss.resourcehandler.symbol.lifetime=2400000
    gss.resourcehandler.symbol.class=com.gesmallworld.gss.lib.resource.ResourceHandler

Den neuen Service für die Rollen *nisNis* und *nisNx* in den *gss-role-nisNis.properties* resp. *gss-role-nisNx.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" ergänzen:

.. code-block:: java

    services=ejb/NisMapLocal, ejb/StaviObjectDetailLocal

Stavi Thema
^^^^^^^^^^^

Im Smallworld muss ein neues Thema mit dem Namen Stavi erfasst werden (Das Thema muss vorhanden sein, jedoch nicht zwingend im nisXplorer als Ebene eingebunden sein).
Eventuell müssen die Sichtbarkeiten für Messstellen angepasst werden. Neben den Messstellen auch die Stationen im Thema aufnehmen, wenn AOs/Messstellen eines Trafokreises selektieren werden sollen (`NX-11142 <https://agile4nis.atlassian.net/browse/NX-11142>`_).

.. image:: avis_thema.png

Update Service
--------------

Damit die Messstellen welche den Stavi Projekten zugeordnet sind aktualisiert werden, muss der *StaviMeterPointUpdate* Service konfiguriert werden.
Neue Services müssen in der *nis_gss_basic_config.xml* unter "nis_resource_configuration_{kundenname}\nis_gss_basic_application\resources\base\data" konfiguriert werden:

.. code-block:: xml

    <service name="update_meterpoint" provider="nis_gss_stavi_meter_point_update_service_provider.update_meterpoint" />
    <service name="status_update_meterpoint" provider="nis_gss_stavi_meter_point_update_service_provider.status_update_meterpoint" />

Neue Datenbank Spezifikationen müssen in der *nis_gss_basic_config.xml* unter "nis_resource_configuration_{kundenname}\nis_gss_basic_application\resources\base\data" konfiguriert werden:

.. code-block:: xml
    :linenos:

    <database_specifications>
    	...
    	...
    	<database_specification type="named" name="stavi_meter_points">
    		<dataset name="strom" mode="readonly" alternative="|gss" />
    		<dataset name="admin_collection" mode="readonly" />
    	</database_specification>
    </database_specifications>

Neuen Service in den *gss-native_vs.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" eintragen:

.. code-block:: java

    gss.service.stavi_meterpointupdate = ejb/StaviMeterPointUpdateLocal

Den neuen Service für die Rollen *nisNis* und *nisNx* in den *gss-role-nisNis.properties* resp. *gss-role-nisNx.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" ergänzen:

.. code-block:: java

    services=ejb/StaviMeterPointUpdateLocal

Update Service aufrufen
^^^^^^^^^^^^^^^^^^^^^^^

Der Update Service sollte täglich ausgeführt werden. Gestartet wird der Service über folgende URL:
``gss/native?service=stavi_meterpointupdate&method=updateMeterpoint``

Das Update der Messstellen sollte in der Nacht laufen, damit die Performance des nisXplorers tagsüber nicht beeinträchtigt wird. Idealerweise sollten parallel dazu keine anderen Reset- / Reinit- oder Update-Jobs laufen.

Optional: Logische Gruppe einrichten / Request Timeout erhöhen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Falls das Update der Messstellen über den *StaviMeterPointUpdate* Service zu lange dauert, kann das Request Timeout erhöht werden. Hierzu muss folgendes gemacht werden:

In den *gss-core.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" das Service Bean einer logischen Gruppe zuordnen (es kann auch eine bestehende Gruppe verwendet werden):

.. code-block:: java

    gss.service.group.StaviMeterPointUpdateBean=stavi_meter_point_update

In den *gss-connector.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" die logische Gruppe deklarieren und das Timeout entfernen oder erhöhen:

.. code-block::

    ## comma separated list of logical groups
    logicalGroups=stavi_meter_point_update

    ## request timeout in ms, disable request timeout for this logical group by setting it to 0
    logicalGroups.stavi_meter_point_update.requestTimeout=0

In der *gssagent.xml* unter "nis_customer_composition_{kundenname}\agent" die logische Gruppe einem Image zuweisen:

.. code-block:: xml

    <connection port="1234" autostart="true" max_channels="2" group="stavi_meter_point_update"/>

SOAP WebServices
----------------

Neuen Service in den *gss-native_vs.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" eintragen:

.. code-block:: java

    gss.service.zavis_nis_isu=ejb/ZAVIS_NIS_ISULocal
    gss.service.zavis_isu_nis=ejb/UpdateProjectLocal

Den neuen Service für die Rollen *nisNis* und *nisNx* in den *gss-role-nisNis.properties* resp. *gss-role-nisNx.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" *ergänzen*:

.. code-block:: java

    services=ejb/ZAVIS_NIS_ISULocal

Den neuen Service für die Rollen *nisSap* in den *gss-role-nisSap.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" *ergänzen*:

.. code-block:: java

    services=ejb/UpdateProjectLocal

Den neuen Web Service in den *gss-web_service_vs.properties* unter "nis_resource_configuration_{kundenname}\nis_gss_config" *ergänzen und konfigurieren*:

.. code-block:: java

    // Die Daten müssen eventuell noch angepasst werden!!!
    gss.sap_bkw_stavi_username=RFC_NIS
    gss.sap_bkw_stavi_password=2018Nis!
    gss.sap_bkw_stavi_endpoint=http://du1dbs.bkw-fmb.ch:8000/sap/bc/srt/wsdl/flv_10002A111AD1/bndg_url/sap/bc/srt/rfc/sap/zavis_nis_isu/100/zavis_nis_isu/http?sap-client=100

**sap User - Benutzer**

Sollte der Kunde noch keinen "sap User" haben muss dieser zwingend angelegt werden.

Magik
-----

Um die Messstellen im Netzschema Kontext anzuzeigen, muss folgendes Client-Setting angepasst werden.

Die Methode nis_gss_get_relations() und den Aufruf von _self.get_wp_objects() mit dem Parameter base_view? erweitern.

.. code-block::
    :linenos:

    _pragma (classify_level=restricted,  topic={nis, nis_gss_object_context_service, client_setting},  usage={internal, subclassable})
    _method nis_sch_repres_record.nis_gss_get_relations(an_ace, ace_visibility_tag, depth, _optional base_rec, base_rel, base_view?)
        # Author       : NIS AG (Markus Jung)
        # Date         : 2015-09-30
        #
        ## Parameters  : an_ace: the ace of the application
        ##             : ace_visibility_tag:  The ace visibility tag
        ##             : depth: the current depth of recursions
        ##             : base_rec: the record, from which the object_context request initially started
        ##
        ## Returns     : HashTable with informations of relations
        ##             : How to deal with default relations (join_fields) :replace if no defaults should be used or :append if the specific relations should be appended to defaults
        ##             : Maximum depth
        ## Description : Get all the relations of self and get the needed informations
        ##               (object_urn, description)
        #

        l_rel << l_rel.default(property_list.new())
        l_trans << l_trans.default(property_list.new())
        _if _self.responds_to?(:|get_wp_objects()|)
        _then
            _local l_wp_objects << _self.get_wp_objects(base_view?)

            _for k, v _over l_wp_objects.fast_keys_and_elements()
            _loop
                l_rel[k] << v
            _endloop
        _endif
        _return l_rel, l_trans
    _endmethod
    $

In der Methode nis_sch_line.get_wp_objects() den Parameter base_view? ergänzen und folgenden Code hinzufügen.

.. code-block::
    :linenos:

    _if _not base_view?
        _then
        ## loop over linked traces of cables
        _for l_el_trasse _over l_wp_obj.nis_wp_links().elements()
        _loop
            ## loop over connected things of trace
            _for l_el_trasse_thing_elem _over l_el_trasse.get_connected_things().elements()
            _loop
                ## loop over objects of things
                _for l_el_trasse_thing_object _over l_el_trasse_thing_elem.get_objects().elements()
                _loop
                    if l_el_trasse_thing_object.nis_class_name _is :nis_el_house_service
                    ## if object is Hansanschluss
                    _then
                        _for l_meterpoint _over l_el_trasse_thing_object.nis_el_meter_points.elements()
                        _loop
                            _if l_wp_objects.includes_key?(l_meterpoint.external_name.as_symbol())
                            _then
                                l_wp_objects[l_meterpoint.external_name.as_symbol()].add(l_meterpoint)
                            _else
                                l_wp_objects[l_meterpoint.external_name.as_symbol()] << rwo_set.new_with(l_meterpoint)
                            _endif
                        _endloop
                    _endif
                _endloop
            _endloop
        _endloop
    _endif

Excel-Export
^^^^^^^^^^^^

Damit auch der Excel-Export funktioniert, muss zusätzlich die Objektbrowser Datenbank mit den Daten von Messstellen erweitert werden. Dazu unter nis_resource_configuration_ibw\nis_gss_object_browser_service\resources\base die Datei mapping_objectbrowser.csv öffnen und mit folgender Zeile erweitern:

.. code-block:: sh

    ...
    strom;nis_el_meter_point;nis_number,kf_ao_street,kf_ao_house_number,kf_ao_plz,kf_ao_community,kf_ao_type,kf_ao_status,kf_ao_id;position

Danach mit dem Aufruf von localhost:8080/gss/native?service=object_browser&method=reinit die Datenbank neu initialisieren.

Projektliste (Paging, Filterung)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Für die Projektliste können folgende Konfigurationen vorgenommen werden in grails_project.groovy:

.. code-block:: java

    nis.stavi.projectsGrid.pageSize = 5 // Anzahl der Projekte, die initial pro Seite angezeigt werden sollen
    nis.stavi.projectsGrid.pageSizeValues = [5, 10, 20] // Auswahlmöglichkeiten wieviele Projekte pro Seite angezeigt werden können

Damit die Filterung der Projekte funktioniert, kann folgende Konfiguration vorgenommen werden:

.. code-block:: java

    nis.stavi.filters = [
        "avi_state.name": [
            type: "list",
            options: ["In Erfassung", "Freigabe pendent", "Freigegeben"], // Verfügbare Filter
            value: ["In Erfassung"] // standardmässig ausgewählter Filter
        ],
        "region.name": [
            type: "list",
            options: [], // braucht es bei Regionen nicht unbedingt, bei leeren options werden alle möglichen Werte ausgelesen
            value: []
        ],
        "project_leader.name": [
            type: "list",
            options: [], // braucht es bei Projektleitern nicht unbedingt, bei leeren options werden alle möglichen Werte ausgelesen
            value: []
        ],
        "type.name": [ // ab 1.1.0-519 und nur für CKW/BKW
            type   : "list", // Auswahlfilter "list" oder "select"
            options: [], // Bei leeren Optionen werden alle möglichen Werte ausgelesen. Falls "Type" ein Pflichtfeld ist, werden die Enumeratoren angezeigt, falls "Type" optional ist zusätzlich noch "Leere anzeigen"
            value  : [] // Standardfilter, zB "Stromabschaltung" oder ["Stromabschaltung"]
        ],
        "insert_user.displayname": [
            type: "list",
            options: ["Peter Muster", "Hans Muster"],
            value: []
        ],
        "archived": [
            type: "list",
            options: ["Ja", "Nein"],
            value: ["Nein"]   // Mit ["Nein"] werden nur die nicht archivierten Projekte angezeigt; ["Ja", "Nein"] -> zeigt alle Projekte an inkl. der Archivierten.
        ]
    ]

Dabei werden alle Spalten, die nicht eingetragen werden, über den standardmässigen Text-Filter gefiltert.

Hier alle verfügbaren Filternamen:

+------------------+-------------------------+
| Spaltenname      | Filtername              |
+==================+=========================+
| Titel            | title                   |
+------------------+-------------------------+
| Auftragsleiter   | region.name             |
+------------------+-------------------------+
| Gebiet           | project_leader.name     |
+------------------+-------------------------+
| Erstellt am      | create_date             |
+------------------+-------------------------+
| Erstellt von     | insert_user.displayname |
+------------------+-------------------------+
| Status           | avi_state.name          |
+------------------+-------------------------+
| Typ              | type.name               |
+------------------+-------------------------+

Behandlung von gelöschten Messstellen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Damit gelöschte Messstellen auf der Karte nicht dargestellt werden sollen muss in der GRAILS Konfiguration (grails_project.groovy)  folgendes Attribut gesetzt werden:

.. code-block:: java

    nis.stavi.meterpoint_state_exclusion = [
        "attribute": "kf_ao_status",
        "value": "Gelöscht"
    ]

Der Block "nis.stavi.meterpoint_state_exclusion" im grails_project.groovy muss zwingend vor dem "nis.stavi.meterpoint_attribute_mapping"-Block definiert werden.

Für "attribute" wird das Attribut auf dem Messstellen Objekt angegeben welches den Status beinhaltet.

Für "value" wird der Wert gesetzt welche Messstellen ausgeschlossen werden wen das Attribut den gesetzten Wert besitzt.

Zudem muss das Status Attribute dem MeterPointMapping hinzugefügt werden:

.. code-block:: java

    nis.stavi.meterpoint_attribute_mapping = [
        ...,
        ...,
        ...,
        "state": nis.stavi.meterpoint_state_exclusion.attribute
    ]

Damit Messstellen von bereits existierenden Projekten als gelöscht markiert werden muss in der MAGIK Klasse "nis_gss_stavi_meter_point_update_config"  die folgende Konstante definiert werden. Dabei beinhaltet das Objekt die gleichen Werte wie in der GRAILS Konfiguration.

.. code-block:: java
    :linenos:

    _pragma (classify_level=basic,  topic={nis, nis_gss_art_service, client_setting},  usage={external})
    nis_gss_stavi_meter_point_update_config.define_shared_constant(
        :state_exclusion,
        property_list.new_with(
            :attribute, :kf_ao_status,
            :value, "Gelöscht"
                 ),
        :public
    )
    $

Archivieren von Projekten
^^^^^^^^^^^^^^^^^^^^^^^^^

Damit Projekte nach der gewünschten Zeit archiviert werden muss in folgendem Verzeichnis "nis\nis_projekte\R43010\{kunde}\nis_resource_configuration_{kunde}\nis_gss_tools\resources\base\db_scripts\stavi_project_archive" folgende Schritte ausgeführt werden:

1.  In der Datei *archive_projects.sql* die gewünschte Zeit (Anzahl Tage) nach der abgeschlossene Projekte archiviert werden sollen anpassen (Standard: 5 Tage)

2.  Die Batch Datei *run_archive_projects.bat* Zeit gesteuert in dem gewünschten Zeitabstand ausführen.

**Optional**

Damit Projekte mit einem anderen Status archiviert werden können, muss in archive_projects.sql die WHERE-Condition angepasst werden. Beispiel für Status 3:

.. code-block:: sql

    UPDATE stavi.project SET archived = TRUE WHERE (avi_state_id = 3) AND (TO_DATE(update_date, 'DD.MM.YYYY') < (CURRENT_DATE - INTERVAL '5 days'));

Objektinformation
-----------------

**Optional**

In der Objektinfo einer Messstelle werden die Avisierungen aufgelistet, in welchen sich die Messstelle befindet.
Über das Grails-Config-Attribut nis.stavi.assigned_projects_states kann angegeben werden, welche Status der Avisierungen berücksichtigt werden. (Standardmässig nur Status 3)

Bsp.:

.. code-block:: java

    nis.stavi.assigned_projects_states = [
        3, // Freigegeben
        4, // Versendet
        5 // Abgebrochen
    ]

Attribute
---------

Damit nicht bei allen Kunden alle Attribute angezeigt werden, muss in der Grails Config das Property *nis.stavi.attributes* hinzugefügt werden. Dabei muss bei jedem Attribut angegeben werden, ob dieses im Client erscheinen soll oder nicht. Beim 'title' Attribut, kann zusätzlich die maximale Länge für die Validierung des Formular Feldes konfiguriert werden.

Hier das Beispiel mit allen Attributen wie es für die *CKW* und *BKW* konfiguriert werden muss. Beim type können folgende Optionen definiert werden:

*   required: optional, true falls Pflichtfeld

*   defaultValue: optional, einer der Typen aus Postgres, defaultmässig Stromabschaltung oder Baustelleninformation

.. code-block:: java

    nis.stavi.attributes = [
        title: [
            visible: true,
            maxLength: 50
        ],
        description: [visible: true],
        type: [visible: true, required: true, defaultValue: "Stromabschaltung"] // ab 1.1.0-519
        work_location: [visible: false],
        project_leader: [visible: true],
        region: [visible: true],
        incident_location: [visible: false],
        cause: [visible: false],
        id: [visible: true],
        create_date: [visible: true],
        insert_user: [visible: true],
        update_date: [visible: true],
        update_user: [visible: true],
        state: [visible: true],
        archived: [visible: true],
        project_dates: [visible: true],
        textblocks: [visible: true]
    ]

Für *EKZ muss *alles ausser type* auf *true* gesetzt werden und das *maxLength* Attribut soll unter dem *title* Attribut nicht konfiguriert werden. *type* wird auf false gesetzt und *required* sowie *defaultValue* nicht konfiguriert.

**Optional (nur EKZ)**

Damit im Client das Attribut *Gebiet* umbenannt wird, können folgende Konfigurationen vorgenommen werden:

*   In der Grails Config das Property *nis.messageCodes* folgendermassen hinzufügen oder ergänzen:

.. code-block::

   nis.messageCodes = [
      'nis.stavi.region'
   ]

*   In den Grails Übersetzungsfiles (*i18n/\*.properties*) die gewünschte Übersetzung für die entsprechende Sprache eintragen. Hier am Beispiel von *de*:

.. code-block:: java

   nis.stavi.region=Organisation

Damit im Client im Hinweis die richtigen obligatorischen Attribute erscheinen, wenn die Avisierung nicht freigegeben werden kann, muss die folgende Übersetzung in den Übersetzungsfiles (*i18n/\*.properties*) angepasst werden. Hier am Beispiel von *de*:

.. code-block:: java

    "nis.stavi.trigger_avi_not_enabled": "Die Avisierung kann noch nicht zum Export freigegeben werden.\nFolgende Felder müssen für einen Export gesetzt sein:\nAnlageverantwortlicher\nOrganisation\nEreignisort\nUrsache\nArbeitsort\nAbstellzeiten\nAnschlussobjekte",

Export von Avisierungslisten
----------------------------

Für den Export der Avisierungslisten muss der Servicetype definiert werden. Für BKW:

.. code-block:: java

    nis.stavi.export_service_type = "sap_bkw"

Oder für EKZ:

.. code-block:: java

    nis.stavi.export_service_type = "excel_ekz"

**Optional (nur EKZ): Template für Excelexport**

Für den Export von Avisierungslisten als Excelfile muss ein Template erstellt und in der Konfiguration verlinkt werden:

.. code-block:: java

    nis.avi.exportTemplate = "D:/nis/nis_projekte/R5/config_02/ibw/nis_web/custom_files/avisierung_template.xlsx"

Das Template darf folgende Parameter enthalten, welche beim Export durch die Werte im Projekt ersetzt werden:

*   *nis_avi_id*: Projekt ID
*   *nis_avi_create_date*: Projekt Erstellungsdatum 'Montag 13. Juni 2016'
*   *nis_avi_title*: Projekt Titel
*   *nis_avi_description*: Projekt Beschreibung
*   *nis_avi_community*: Gemeinde des ersten Anschlussprojekts
*   *nis_avi_region*: Projekt Region/Organisation
*   *nis_avi_work_location*: Projekt Arbeitsort
*   *nis_avi_incident_location*: Projekt Ereignisort
*   *nis_avi_cause*: Projekt Ursache
*   *nis_avi_street*: Anschlussobjekt Strasse
*   *nis_avi_street_addition*: Anschlussobjekt Strasse Zusatz
*   *nis_avi_house_number*: Anschlussobjekt Hausnummer
*   *nis_avi_art*: Anschlussobjekt Art
*   *nis_avi_number_of_customers*: Anschlussobjekt Kundenzahl
*   *nis_avi_customer_sum*: Summe der Kundenzahl aller Anschlussobjekte
*   *nis_avi_tickets_sum*:  Anzahl benötigter Zettel
*   *nis_avi_down_start_date*: Abstellzeit Startdatum '25.03.2018'
*   *nis_avi_down_start_time*: Abstellzeit Startzeit '09:00'
*   *nis_avi_down_end_date*: Abstellzeit Enddatum '25.03.2018'
*   *nis_avi_down_end_time*: Abstellzeit Endzeit '13:00'

*nis_avi_street*, *nis_avi_street_addition*, *nis_avi_house_number*, *nis_avi_art* und *nis_avi_number_of_customers* müssen in derselben Zeile stehen, da pro Anschlussobjekt diese Zeile kopiert und abgefüllt wird.

*nis_avi_down_start_date*, *nis_avi_down_start_time*, *nis_avi_down_end_date* und *nis_avi_down_end_time* müssen in derselben Zeile stehen, da pro Abstellzeit (max. 4 Zeiten) diese Zeile kopiert und abgefüllt wird.

Der Parameter *nis_avi_customer_sum* kann in Formeln integriert werden. Wichtig dabei ist, dass das Formelfeld als Text formatiert ist und am Anfang der Formel kein '=' ist. Beispiel: 'nis_avi_customer_sum * 2'

Unterhalb der Zeilen für Anschlussobjekte und Abstellzeiten dürfen keine Bilder und Checkboxen sein. Als Checkbox-Alternative wird eine umrahmte Zelle mit einem 'x' als Häckchen empfohlen.